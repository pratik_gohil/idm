// dont remove any function from this file if want application to be installable

self.addEventListener('install', evt => {
 // install serviceworker
 console.log('service worker is installed')
})


self.addEventListener('activate', evt => {
 // activate service worker
 console.log('service worker has been activated')
})

self.addEventListener('fetch', evt => {
 // you can cache files/assets here
 // console.log('fetch event', evt)
})